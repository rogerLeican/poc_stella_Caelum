package com.poc.stella_alura.controller.v1.beanvalidation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.controller.v1.beanvalidation.stub.invalidos.EmpresaBeanValidationInvalidArgumentProviderStub;
import com.poc.stella_alura.controller.v1.beanvalidation.stub.validos.EmpresaBeanValidationValidArgumentProviderStub;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmpresaRestControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @ParameterizedTest
    @ArgumentsSource(EmpresaBeanValidationValidArgumentProviderStub.class)
    @DisplayName("Deve salvar empresas com inscrição estadual valida ")
    void deveSalvarEmpresasComInscricaoEstadualValida(EmpresaContract empresa) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        var bodyExpect = mapper.writeValueAsString(empresa);
        webTestClient.post()
                .uri("/v1/empresa")
                .body(BodyInserters.fromObject(empresa))
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .json(bodyExpect);
    }

    @ParameterizedTest
    @ArgumentsSource(EmpresaBeanValidationInvalidArgumentProviderStub.class)
    @DisplayName("Deve Lancar Excecao Quando Receber Empresas Com Inscricao Estadual Invalida")
    void deveLancarExcecaoQuandoReceberEmpresasComInscricaoEstadualInvalida(EmpresaContract empresa)
            throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        var bodyExpect = mapper.writeValueAsString(empresa);
        webTestClient.post()
                .uri("/v1/empresa")
                .body(BodyInserters.fromObject(empresa))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
}