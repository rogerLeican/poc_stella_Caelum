package com.poc.stella_alura.controller.v1.stellacore.stubs.validos;

import static com.poc.stella_alura.controller.v1.stellacore.stubs.validos.EmpresasStellaCoreComIEValidosStub.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class EmpresaValidArgumentProviderStub implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                Arguments.of(stubEmpresaStellaCoreContractAC()),
                Arguments.of(stubEmpresaStellaCoreContractAL()),
                Arguments.of(stubEmpresaStellaCoreContractAP()),
                Arguments.of(stubEmpresaStellaCoreContractAM()),
                Arguments.of(stubEmpresaStellaCoreContractBA()),
                Arguments.of(stubEmpresaStellaCoreContractCE()),
                Arguments.of(stubEmpresaStellaCoreContractDF()),
                Arguments.of(stubEmpresaStellaCoreContractES()),
                Arguments.of(stubEmpresaStellaCoreContractGO()),
                Arguments.of(stubEmpresaStellaCoreContractMA()),
                Arguments.of(stubEmpresaStellaCoreContractMS()),
                Arguments.of(stubEmpresaStellaCoreContractMT()),
                Arguments.of(stubEmpresaStellaCoreContractMG()),
                Arguments.of(stubEmpresaStellaCoreContractPA()),
                Arguments.of(stubEmpresaStellaCoreContractPB()),
                Arguments.of(stubEmpresaStellaCoreContractPR()),
                Arguments.of(stubEmpresaStellaCoreContractPE()),
                Arguments.of(stubEmpresaStellaCoreContractPI()),
                Arguments.of(stubEmpresaStellaCoreContractRJ()),
                Arguments.of(stubEmpresaStellaCoreContractRN()),
                Arguments.of(stubEmpresaStellaCoreContractRS()),
                Arguments.of(stubEmpresaStellaCoreContractRO()),
                Arguments.of(stubEmpresaStellaCoreContractRR()),
                Arguments.of(stubEmpresaStellaCoreContractSC()),
                Arguments.of(stubEmpresaStellaCoreContractSP()),
                Arguments.of(stubEmpresaStellaCoreContractSE()),
                Arguments.of(stubEmpresaStellaCoreContractTO())
        );
    }
}
