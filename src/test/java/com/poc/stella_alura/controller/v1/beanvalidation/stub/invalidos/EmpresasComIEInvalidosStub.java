package com.poc.stella_alura.controller.v1.beanvalidation.stub.invalidos;

import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;

public class EmpresasComIEInvalidosStub {

    public static EmpresaContract stubEmpresaContractInvalidAC() {
        return EmpresaContract.builder()
                .estado("AC")
                .ie("0280225223710")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidAL() {
        return EmpresaContract.builder()
                .estado("AL")
                .ie("228150367")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidAP() {
        return EmpresaContract.builder()
                .estado("AP")
                .ie("047283448")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidAM() {
        return EmpresaContract.builder()
                .estado("AM")
                .ie("2458841455")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidBA() {
        return EmpresaContract.builder()
                .estado("BA")
                .ie("08670053")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidCE() {
        return EmpresaContract.builder()
                .estado("CE")
                .ie("275585610")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidDF() {
        return EmpresaContract.builder()
                .estado("DF")
                .ie("0702623700101")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidES() {
        return EmpresaContract.builder()
                .estado("ES")
                .ie("362620767")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidGO() {
        return EmpresaContract.builder()
                .estado("GO")
                .ie("103806432")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidMA() {
        return EmpresaContract.builder()
                .estado("MA")
                .ie("124488586")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidMS() {
        return EmpresaContract.builder()
                .estado("MS")
                .ie("286260963")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidMT() {
        return EmpresaContract.builder()
                .estado("MT")
                .ie("02037219321")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidMG() {
        return EmpresaContract.builder()
                .estado("MG")
                .ie("3948450706836")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidPA() {
        return EmpresaContract.builder()
                .estado("PA")
                .ie("156696230")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidPB() {
        return EmpresaContract.builder()
                .estado("PB")
                .ie("045454514")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidPR() {
        return EmpresaContract.builder()
                .estado("PR")
                .ie("7908711543")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidPE() {
        return EmpresaContract.builder()
                .estado("PE")
                .ie("249816220")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidPI() {
        return EmpresaContract.builder()
                .estado("PI")
                .ie("712946593")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidRJ() {
        return EmpresaContract.builder()
                .estado("RJ")
                .ie("57514580")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidRN() {
        return EmpresaContract.builder()
                .estado("RN")
                .ie("208244343")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidRS() {
        return EmpresaContract.builder()
                .estado("RS")
                .ie("0145389412")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidRO() {
        return EmpresaContract.builder()
                .estado("RO")
                .ie("30282630889114")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidRR() {
        return EmpresaContract.builder()
                .estado("RR")
                .ie("249633583")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidSC() {
        return EmpresaContract.builder()
                .estado("SC")
                .ie("014560536")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidSP() {
        return EmpresaContract.builder()
                .estado("SP")
                .ie("891561080752")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidSE() {
        return EmpresaContract.builder()
                .estado("SE")
                .ie("315636113")
                .build();
    }

    public static EmpresaContract stubEmpresaContractInvalidTO() {
        return EmpresaContract.builder()
                .estado("TO")
                .ie("85032692945")
                .build();
    }
}
