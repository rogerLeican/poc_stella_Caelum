package com.poc.stella_alura.controller.v1.stellacore.stubs.validos;

import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EmpresasStellaCoreComIEValidosStub {

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractAC() {
        return EmpresaStellaCoreContract.builder()
                .estado("AC")
                .ie("0180225223710")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractAL() {
        return EmpresaStellaCoreContract.builder()
                .estado("AL")
                .ie("248150367")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractAP() {
        return EmpresaStellaCoreContract.builder()
                .estado("AP")
                .ie("037283448")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractAM() {
        return EmpresaStellaCoreContract.builder()
                .estado("AM")
                .ie("458841455")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractBA() {
        return EmpresaStellaCoreContract.builder()
                .estado("BA")
                .ie("08670054")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractCE() {
        return EmpresaStellaCoreContract.builder()
                .estado("CE")
                .ie("275585611")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractDF() {
        return EmpresaStellaCoreContract.builder()
                .estado("DF")
                .ie("0702623700191")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractES() {
        return EmpresaStellaCoreContract.builder()
                .estado("ES")
                .ie("362620768")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractGO() {
        return EmpresaStellaCoreContract.builder()
                .estado("GO")
                .ie("103806431")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractMA() {
        return EmpresaStellaCoreContract.builder()
                .estado("MA")
                .ie("124488587")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractMS() {
        return EmpresaStellaCoreContract.builder()
                .estado("MS")
                .ie("286260964")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractMT() {
        return EmpresaStellaCoreContract.builder()
                .estado("MT")
                .ie("02037219320")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractMG() {
        return EmpresaStellaCoreContract.builder()
                .estado("MG")
                .ie("3948450706837")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractPA() {
        return EmpresaStellaCoreContract.builder()
                .estado("PA")
                .ie("156696240")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractPB() {
        return EmpresaStellaCoreContract.builder()
                .estado("PB")
                .ie("045454515")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractPR() {
        return EmpresaStellaCoreContract.builder()
                .estado("PR")
                .ie("7908711542")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractPE() {
        return EmpresaStellaCoreContract.builder()
                .estado("PE")
                .ie("249816210")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractPI() {
        return EmpresaStellaCoreContract.builder()
                .estado("PI")
                .ie("712946594")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractRJ() {
        return EmpresaStellaCoreContract.builder()
                .estado("RJ")
                .ie("57514590")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractRN() {
        return EmpresaStellaCoreContract.builder()
                .estado("RN")
                .ie("208244344")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractRS() {
        return EmpresaStellaCoreContract.builder()
                .estado("RS")
                .ie("0145389413")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractRO() {
        return EmpresaStellaCoreContract.builder()
                .estado("RO")
                .ie("30282630889113")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractRR() {
        return EmpresaStellaCoreContract.builder()
                .estado("RR")
                .ie("249633584")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractSC() {
        return EmpresaStellaCoreContract.builder()
                .estado("SC")
                .ie("014560526")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractSP() {
        return EmpresaStellaCoreContract.builder()
                .estado("SP")
                .ie("891561080753")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractSE() {
        return EmpresaStellaCoreContract.builder()
                .estado("SE")
                .ie("315636114")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractTO() {
        return EmpresaStellaCoreContract.builder()
                .estado("TO")
                .ie("85032692935")
                .build();
    }

}
