package com.poc.stella_alura.controller.v1.beanvalidation.stub.validos;

import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;

public class EmpresasContractStub {

    public static EmpresaContract stubEmpresaContractAC() {
        return EmpresaContract.builder()
                .estado("AC")
                .ie("0180225223710")
                .build();
    }

    public static EmpresaContract stubEmpresaContractAL() {
        return EmpresaContract.builder()
                .estado("AL")
                .ie("248150367")
                .build();
    }

    public static EmpresaContract stubEmpresaContractAP() {
        return EmpresaContract.builder()
                .estado("AP")
                .ie("037283448")
                .build();
    }

    public static EmpresaContract stubEmpresaContractAM() {
        return EmpresaContract.builder()
                .estado("AM")
                .ie("458841455")
                .build();
    }

    public static EmpresaContract stubEmpresaContractBA() {
        return EmpresaContract.builder()
                .estado("BA")
                .ie("08670054")
                .build();
    }

    public static EmpresaContract stubEmpresaContractCE() {
        return EmpresaContract.builder()
                .estado("CE")
                .ie("275585611")
                .build();
    }

    public static EmpresaContract stubEmpresaContractDF() {
        return EmpresaContract.builder()
                .estado("DF")
                .ie("0702623700191")
                .build();
    }

    public static EmpresaContract stubEmpresaContractES() {
        return EmpresaContract.builder()
                .estado("ES")
                .ie("362620768")
                .build();
    }

    public static EmpresaContract stubEmpresaContractGO() {
        return EmpresaContract.builder()
                .estado("GO")
                .ie("103806431")
                .build();
    }

    public static EmpresaContract stubEmpresaContractMA() {
        return EmpresaContract.builder()
                .estado("MA")
                .ie("124488587")
                .build();
    }

    public static EmpresaContract stubEmpresaContractMS() {
        return EmpresaContract.builder()
                .estado("MS")
                .ie("286260964")
                .build();
    }

    public static EmpresaContract stubEmpresaContractMT() {
        return EmpresaContract.builder()
                .estado("MT")
                .ie("02037219320")
                .build();
    }

    public static EmpresaContract stubEmpresaContractMG() {
        return EmpresaContract.builder()
                .estado("MG")
                .ie("3948450706837")
                .build();
    }

    public static EmpresaContract stubEmpresaContractPA() {
        return EmpresaContract.builder()
                .estado("PA")
                .ie("156696240")
                .build();
    }

    public static EmpresaContract stubEmpresaContractPB() {
        return EmpresaContract.builder()
                .estado("PB")
                .ie("045454515")
                .build();
    }

    public static EmpresaContract stubEmpresaContractPR() {
        return EmpresaContract.builder()
                .estado("PR")
                .ie("7908711542")
                .build();
    }

    public static EmpresaContract stubEmpresaContractPE() {
        return EmpresaContract.builder()
                .estado("PE")
                .ie("249816210")
                .build();
    }

    public static EmpresaContract stubEmpresaContractPI() {
        return EmpresaContract.builder()
                .estado("PI")
                .ie("712946594")
                .build();
    }

    public static EmpresaContract stubEmpresaContractRJ() {
        return EmpresaContract.builder()
                .estado("RJ")
                .ie("57514590")
                .build();
    }

    public static EmpresaContract stubEmpresaContractRN() {
        return EmpresaContract.builder()
                .estado("RN")
                .ie("208244344")
                .build();
    }

    public static EmpresaContract stubEmpresaContractRS() {
        return EmpresaContract.builder()
                .estado("RS")
                .ie("0145389413")
                .build();
    }

    public static EmpresaContract stubEmpresaContractRO() {
        return EmpresaContract.builder()
                .estado("RO")
                .ie("30282630889113")
                .build();
    }

    public static EmpresaContract stubEmpresaContractRR() {
        return EmpresaContract.builder()
                .estado("RR")
                .ie("249633584")
                .build();
    }

    public static EmpresaContract stubEmpresaContractSC() {
        return EmpresaContract.builder()
                .estado("SC")
                .ie("014560526")
                .build();
    }

    public static EmpresaContract stubEmpresaContractSP() {
        return EmpresaContract.builder()
                .estado("SP")
                .ie("891561080753")
                .build();
    }

    public static EmpresaContract stubEmpresaContractSE() {
        return EmpresaContract.builder()
                .estado("SE")
                .ie("315636114")
                .build();
    }

    public static EmpresaContract stubEmpresaContractTO() {
        return EmpresaContract.builder()
                .estado("TO")
                .ie("85032692935")
                .build();
    }

}
