package com.poc.stella_alura.controller.v1.beanvalidation.stub.invalidos;

import static com.poc.stella_alura.controller.v1.beanvalidation.stub.invalidos.EmpresasComIEInvalidosStub.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class EmpresaBeanValidationInvalidArgumentProviderStub implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                Arguments.of(stubEmpresaContractInvalidAC()),
                Arguments.of(stubEmpresaContractInvalidAL()),
                Arguments.of(stubEmpresaContractInvalidAP()),
                Arguments.of(stubEmpresaContractInvalidAM()),
                Arguments.of(stubEmpresaContractInvalidBA()),
                Arguments.of(stubEmpresaContractInvalidCE()),
                Arguments.of(stubEmpresaContractInvalidDF()),
                Arguments.of(stubEmpresaContractInvalidES()),
                Arguments.of(stubEmpresaContractInvalidGO()),
                Arguments.of(stubEmpresaContractInvalidMA()),
                Arguments.of(stubEmpresaContractInvalidMS()),
                Arguments.of(stubEmpresaContractInvalidMT()),
                Arguments.of(stubEmpresaContractInvalidMG()),
                Arguments.of(stubEmpresaContractInvalidPA()),
                Arguments.of(stubEmpresaContractInvalidPB()),
                Arguments.of(stubEmpresaContractInvalidPR()),
                Arguments.of(stubEmpresaContractInvalidPE()),
                Arguments.of(stubEmpresaContractInvalidPI()),
                Arguments.of(stubEmpresaContractInvalidRJ()),
                Arguments.of(stubEmpresaContractInvalidRN()),
                Arguments.of(stubEmpresaContractInvalidRS()),
                Arguments.of(stubEmpresaContractInvalidRO()),
                Arguments.of(stubEmpresaContractInvalidRR()),
                Arguments.of(stubEmpresaContractInvalidSC()),
                Arguments.of(stubEmpresaContractInvalidSP()),
                Arguments.of(stubEmpresaContractInvalidSE()),
                Arguments.of(stubEmpresaContractInvalidTO())
        );
    }
}
