package com.poc.stella_alura.controller.v1.beanvalidation.stub.validos;

import static com.poc.stella_alura.controller.v1.beanvalidation.stub.validos.EmpresasContractStub.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class EmpresaBeanValidationValidArgumentProviderStub implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                Arguments.of(stubEmpresaContractAC()),
                Arguments.of(stubEmpresaContractAL()),
                Arguments.of(stubEmpresaContractAP()),
                Arguments.of(stubEmpresaContractAM()),
                Arguments.of(stubEmpresaContractBA()),
                Arguments.of(stubEmpresaContractCE()),
                Arguments.of(stubEmpresaContractDF()),
                Arguments.of(stubEmpresaContractES()),
                Arguments.of(stubEmpresaContractGO()),
                Arguments.of(stubEmpresaContractMA()),
                Arguments.of(stubEmpresaContractMS()),
                Arguments.of(stubEmpresaContractMT()),
                Arguments.of(stubEmpresaContractMG()),
                Arguments.of(stubEmpresaContractPA()),
                Arguments.of(stubEmpresaContractPB()),
                Arguments.of(stubEmpresaContractPR()),
                Arguments.of(stubEmpresaContractPE()),
                Arguments.of(stubEmpresaContractPI()),
                Arguments.of(stubEmpresaContractRJ()),
                Arguments.of(stubEmpresaContractRN()),
                Arguments.of(stubEmpresaContractRS()),
                Arguments.of(stubEmpresaContractRO()),
                Arguments.of(stubEmpresaContractRR()),
                Arguments.of(stubEmpresaContractSC()),
                Arguments.of(stubEmpresaContractSP()),
                Arguments.of(stubEmpresaContractSE()),
                Arguments.of(stubEmpresaContractTO())
        );
    }

}
