package com.poc.stella_alura.controller.v1.stellacore.stubs.invalidos;

import static com.poc.stella_alura.controller.v1.stellacore.stubs.invalidos.EmpresasStellaCoreComIEInvalidosStub.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class EmpresaInvalidArgumentProviderStub implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                Arguments.of(stubEmpresaStellaCoreContractInvalidAC()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidAL()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidAP()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidAM()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidBA()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidCE()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidDF()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidES()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidGO()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidMA()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidMS()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidMT()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidMG()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidPA()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidPB()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidPR()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidPE()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidPI()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidRJ()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidRN()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidRS()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidRO()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidRR()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidSC()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidSP()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidSE()),
                Arguments.of(stubEmpresaStellaCoreContractInvalidTO())
        );
    }
}
