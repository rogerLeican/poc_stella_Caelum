package com.poc.stella_alura.controller.v1.stellacore.stubs.invalidos;

import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;

public class EmpresasStellaCoreComIEInvalidosStub {

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidAC() {
        return EmpresaStellaCoreContract.builder()
                .estado("AC")
                .ie("0280225223710")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidAL() {
        return EmpresaStellaCoreContract.builder()
                .estado("AL")
                .ie("228150367")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidAP() {
        return EmpresaStellaCoreContract.builder()
                .estado("AP")
                .ie("047283448")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidAM() {
        return EmpresaStellaCoreContract.builder()
                .estado("AM")
                .ie("2458841455")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidBA() {
        return EmpresaStellaCoreContract.builder()
                .estado("BA")
                .ie("08670053")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidCE() {
        return EmpresaStellaCoreContract.builder()
                .estado("CE")
                .ie("275585610")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidDF() {
        return EmpresaStellaCoreContract.builder()
                .estado("DF")
                .ie("0702623700101")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidES() {
        return EmpresaStellaCoreContract.builder()
                .estado("ES")
                .ie("362620767")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidGO() {
        return EmpresaStellaCoreContract.builder()
                .estado("GO")
                .ie("103806432")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidMA() {
        return EmpresaStellaCoreContract.builder()
                .estado("MA")
                .ie("124488586")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidMS() {
        return EmpresaStellaCoreContract.builder()
                .estado("MS")
                .ie("286260963")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidMT() {
        return EmpresaStellaCoreContract.builder()
                .estado("MT")
                .ie("02037219321")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidMG() {
        return EmpresaStellaCoreContract.builder()
                .estado("MG")
                .ie("3948450706836")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidPA() {
        return EmpresaStellaCoreContract.builder()
                .estado("PA")
                .ie("156696230")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidPB() {
        return EmpresaStellaCoreContract.builder()
                .estado("PB")
                .ie("045454514")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidPR() {
        return EmpresaStellaCoreContract.builder()
                .estado("PR")
                .ie("7908711543")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidPE() {
        return EmpresaStellaCoreContract.builder()
                .estado("PE")
                .ie("249816220")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidPI() {
        return EmpresaStellaCoreContract.builder()
                .estado("PI")
                .ie("712946593")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidRJ() {
        return EmpresaStellaCoreContract.builder()
                .estado("RJ")
                .ie("57514580")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidRN() {
        return EmpresaStellaCoreContract.builder()
                .estado("RN")
                .ie("208244343")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidRS() {
        return EmpresaStellaCoreContract.builder()
                .estado("RS")
                .ie("0145389412")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidRO() {
        return EmpresaStellaCoreContract.builder()
                .estado("RO")
                .ie("30282630889114")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidRR() {
        return EmpresaStellaCoreContract.builder()
                .estado("RR")
                .ie("249633583")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidSC() {
        return EmpresaStellaCoreContract.builder()
                .estado("SC")
                .ie("014560536")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidSP() {
        return EmpresaStellaCoreContract.builder()
                .estado("SP")
                .ie("891561080752")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidSE() {
        return EmpresaStellaCoreContract.builder()
                .estado("SE")
                .ie("315636113")
                .build();
    }

    public static EmpresaStellaCoreContract stubEmpresaStellaCoreContractInvalidTO() {
        return EmpresaStellaCoreContract.builder()
                .estado("TO")
                .ie("85032692945")
                .build();
    }
}
