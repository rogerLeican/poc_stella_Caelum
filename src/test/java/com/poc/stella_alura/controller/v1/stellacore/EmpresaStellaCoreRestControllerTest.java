package com.poc.stella_alura.controller.v1.stellacore;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import com.poc.stella_alura.controller.v1.stellacore.stubs.invalidos.EmpresaInvalidArgumentProviderStub;
import com.poc.stella_alura.controller.v1.stellacore.stubs.validos.EmpresaValidArgumentProviderStub;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmpresaStellaCoreRestControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @ParameterizedTest
    @ArgumentsSource(EmpresaValidArgumentProviderStub.class)
    @DisplayName("Deve salvar empresas com inscrição estadual valida ")
    void deveSalvarEmpresasComInscricaoEstadualValida(EmpresaStellaCoreContract empresa) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        var bodyExpect = mapper.writeValueAsString(empresa);
        webTestClient.post()
                .uri("/v1/empresa/stellacore")
                .body(BodyInserters.fromObject(empresa))
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .json(bodyExpect);

    }

    @ParameterizedTest
    @ArgumentsSource(EmpresaInvalidArgumentProviderStub.class)
    void salvarEmpresaComIEInvalido(EmpresaStellaCoreContract empresa) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        var bodyExpect = mapper.writeValueAsString(empresa);
        webTestClient.post()
                .uri("/v1/empresa/stellacore")
                .body(BodyInserters.fromObject(empresa))
                .exchange()
                .expectStatus()
                .is4xxClientError();
    }


}