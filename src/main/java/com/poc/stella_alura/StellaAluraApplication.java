package com.poc.stella_alura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StellaAluraApplication {

	public static void main(String[] args) {
		SpringApplication.run(StellaAluraApplication.class, args);
	}

}
