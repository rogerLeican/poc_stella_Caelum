package com.poc.stella_alura.service.stellacore;

import com.poc.stella_alura.service.exception.InscricaoInvalidaException;
import com.poc.stella_alura.service.stellacore.model.EmpresaStellaCore;
import com.poc.stella_alura.service.util.ValidatorIE;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class EmpresaStellaCoreValidationService {



    public Mono<EmpresaStellaCore> salvarEmpresa(EmpresaStellaCore empresa) {


        if (ValidatorIE.validaIE(empresa.getIe(),empresa.getEstado())) {
        }else{
            throw new InscricaoInvalidaException("A inscrição municipal não é valida");
        }


        return Mono.just(empresa);
    }

}
