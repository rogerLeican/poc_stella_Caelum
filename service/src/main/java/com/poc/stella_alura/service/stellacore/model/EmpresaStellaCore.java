package com.poc.stella_alura.service.stellacore.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class EmpresaStellaCore {

    private String razaoSocial;
    private String estado;
    private String ie;
    private String foiValidado;
}
