package com.poc.stella_alura.service.beanvalidation;

import com.poc.stella_alura.service.beanvalidation.model.Empresa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class EmpresaService {

    public Mono<Empresa> salvarEmpresa(Empresa empresa) {
        return Mono.just(empresa);
    }
}
