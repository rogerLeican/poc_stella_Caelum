package com.poc.stella_alura.service.beanvalidation;

import com.poc.stella_alura.service.beanvalidation.model.Empresa;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Component
@AllArgsConstructor
public class EmpresaServiceFacade {

    private final EmpresaService empresaService;

    public Mono<Empresa> salvarEmpresa(@RequestBody @Valid Empresa empresa) {
        return empresaService.salvarEmpresa(empresa);
    }

}
