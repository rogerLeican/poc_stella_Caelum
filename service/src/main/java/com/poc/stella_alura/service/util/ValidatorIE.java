package com.poc.stella_alura.service.util;

import br.com.caelum.stella.validation.ie.IEAcreValidator;
import br.com.caelum.stella.validation.ie.IEAlagoasValidator;
import br.com.caelum.stella.validation.ie.IEAmapaValidator;
import br.com.caelum.stella.validation.ie.IEAmazonasValidator;
import br.com.caelum.stella.validation.ie.IEBahiaValidator;
import br.com.caelum.stella.validation.ie.IECearaValidator;
import br.com.caelum.stella.validation.ie.IEDistritoFederalValidator;
import br.com.caelum.stella.validation.ie.IEEspiritoSantoValidator;
import br.com.caelum.stella.validation.ie.IEGoiasValidator;
import br.com.caelum.stella.validation.ie.IEMaranhaoValidator;
import br.com.caelum.stella.validation.ie.IEMatoGrossoDoSulValidator;
import br.com.caelum.stella.validation.ie.IEMatoGrossoValidator;
import br.com.caelum.stella.validation.ie.IEMinasGeraisValidator;
import br.com.caelum.stella.validation.ie.IEParaValidator;
import br.com.caelum.stella.validation.ie.IEParaibaValidator;
import br.com.caelum.stella.validation.ie.IEParanaValidator;
import br.com.caelum.stella.validation.ie.IEPernambucoValidator;
import br.com.caelum.stella.validation.ie.IEPiauiValidator;
import br.com.caelum.stella.validation.ie.IERioDeJaneiroValidator;
import br.com.caelum.stella.validation.ie.IERioGrandeDoNorteValidator;
import br.com.caelum.stella.validation.ie.IERioGrandeDoSulValidator;
import br.com.caelum.stella.validation.ie.IERondoniaValidator;
import br.com.caelum.stella.validation.ie.IERoraimaValidator;
import br.com.caelum.stella.validation.ie.IESantaCatarinaValidator;
import br.com.caelum.stella.validation.ie.IESaoPauloValidator;
import br.com.caelum.stella.validation.ie.IESergipeValidator;
import br.com.caelum.stella.validation.ie.IETocantinsValidator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidatorIE {

    public static boolean validaIE(String ie, String uf) {

        try {

            if (uf.equals("AC")) {
                new IEAcreValidator(false).assertValid(ie);
            }
            if (uf.equals("AL")) {
                new IEAlagoasValidator(false).assertValid(ie);
            }
            if (uf.equals("AP")) {
                new IEAmapaValidator(false).assertValid(ie);
            }
            if (uf.equals("AM")) {
                new IEAmazonasValidator(false).assertValid(ie);
            }
            if (uf.equals("BA")) {
                new IEBahiaValidator(false).assertValid(ie);
            }
            if (uf.equals("CE")) {
                new IECearaValidator(false).assertValid(ie);
            }
            if (uf.equals("DF")) {
                new IEDistritoFederalValidator(false).assertValid(ie);
            }
            if (uf.equals("ES")) {
                new IEEspiritoSantoValidator(false).assertValid(ie);
            }
            if (uf.equals("GO")) {
                new IEGoiasValidator(false).assertValid(ie);
            }
            if (uf.equals("MA")) {
                new IEMaranhaoValidator(false).assertValid(ie);
            }
            if (uf.equals("MS")) {
                new IEMatoGrossoDoSulValidator(false).assertValid(ie);
            }
            if (uf.equals("MT")) {
                new IEMatoGrossoValidator(false).assertValid(ie);
            }
            if (uf.equals("MG")) {
                new IEMinasGeraisValidator(false).assertValid(ie);
            }
            if (uf.equals("PA")) {
                new IEParaValidator(false).assertValid(ie);
            }
            if (uf.equals("PB")) {
                new IEParaibaValidator(false).assertValid(ie);
            }
            if (uf.equals("PR")) {
                new IEParanaValidator(false).assertValid(ie);
            }
            if (uf.equals("PE")) {
                new IEPernambucoValidator(false).assertValid(ie);
            }
            if (uf.equals("PI")) {
                new IEPiauiValidator(false).assertValid(ie);
            }
            if (uf.equals("RJ")) {
                new IERioDeJaneiroValidator(false).assertValid(ie);
            }
            if (uf.equals("RN")) {
                new IERioGrandeDoNorteValidator(false).assertValid(ie);
            }
            if (uf.equals("RS")) {
                new IERioGrandeDoSulValidator(false).assertValid(ie);
            }
            if (uf.equals("RO")) {
                new IERondoniaValidator(false).assertValid(ie);
            }
            if (uf.equals("RR")) {
                new IERoraimaValidator(false).assertValid(ie);
            }
            if (uf.equals("SC")) {
                new IESantaCatarinaValidator(false).assertValid(ie);
            }
            if (uf.equals("SP")) {
                new IESaoPauloValidator(false).assertValid(ie);
            }
            if (uf.equals("SE")) {
                new IESergipeValidator(false).assertValid(ie);
            }
            if (uf.equals("TO")) {
                new IETocantinsValidator(false).assertValid(ie);
            }

            return true;

        } catch (Exception e) {
            return false;
        }
    }
}
