package com.poc.stella_alura.service.stellacore;

import com.poc.stella_alura.service.stellacore.model.EmpresaStellaCore;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class EmpresaStellaCoreValidationFacade {

    private final EmpresaStellaCoreValidationService service;

    public Mono<EmpresaStellaCore> salvarEmpresa(EmpresaStellaCore empresa) {
        return service.salvarEmpresa(empresa);
    }
}
