package com.poc.stella_alura.controller.v1.stellacore;

import com.poc.stella_alura.controller.v1.beanvalidation.mapper.EmpresaContractMapper;
import com.poc.stella_alura.controller.v1.beanvalidation.mapper.EmpresaContractRequestMapper;
import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.controller.v1.stellacore.mapper.EmpresaStellaCoreContractMapper;
import com.poc.stella_alura.controller.v1.stellacore.mapper.EmpresaStellaCoreContractRequestMapper;
import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import com.poc.stella_alura.service.beanvalidation.EmpresaServiceFacade;
import com.poc.stella_alura.service.stellacore.EmpresaStellaCoreValidationFacade;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Component
@AllArgsConstructor
public class EmpresaStellaCoreContractFacade {

    private final EmpresaStellaCoreValidationFacade serviceFacade;

    public Mono<EmpresaStellaCoreContract> salvarEmpresa(@RequestBody @Valid EmpresaStellaCoreContract empresa) {
        return serviceFacade.salvarEmpresa(
                EmpresaStellaCoreContractRequestMapper.INSTANCE.mapFrom(empresa))
                .map(EmpresaStellaCoreContractMapper.INSTANCE::mapFrom);
    }

}
