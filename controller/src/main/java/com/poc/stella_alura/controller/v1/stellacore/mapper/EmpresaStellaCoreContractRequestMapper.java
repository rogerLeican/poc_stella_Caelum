package com.poc.stella_alura.controller.v1.stellacore.mapper;

import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import com.poc.stella_alura.service.stellacore.model.EmpresaStellaCore;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmpresaStellaCoreContractRequestMapper {

    EmpresaStellaCoreContractRequestMapper INSTANCE = Mappers.getMapper(EmpresaStellaCoreContractRequestMapper.class);

    EmpresaStellaCore mapFrom(EmpresaStellaCoreContract contract);
}
