package com.poc.stella_alura.controller.v1.stellacore.mapper;

import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import com.poc.stella_alura.service.stellacore.model.EmpresaStellaCore;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmpresaStellaCoreContractMapper {

    EmpresaStellaCoreContractMapper INSTANCE = Mappers.getMapper(EmpresaStellaCoreContractMapper.class);

    EmpresaStellaCoreContract mapFrom(EmpresaStellaCore empresa);
}
