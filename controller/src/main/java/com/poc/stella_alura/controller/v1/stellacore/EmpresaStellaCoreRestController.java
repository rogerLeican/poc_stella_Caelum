package com.poc.stella_alura.controller.v1.stellacore;

import com.poc.stella_alura.controller.v1.stellacore.model.EmpresaStellaCoreContract;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1")
@AllArgsConstructor
public class EmpresaStellaCoreRestController {

    private final EmpresaStellaCoreContractFacade empresaFacade;

    @PostMapping("/empresa/stellacore")
    public Mono<EmpresaStellaCoreContract> salvarEmpresa(@RequestBody @Valid EmpresaStellaCoreContract empresa) {
        return empresaFacade.salvarEmpresa(empresa);
    }

}
