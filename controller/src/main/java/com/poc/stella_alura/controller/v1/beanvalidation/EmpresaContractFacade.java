package com.poc.stella_alura.controller.v1.beanvalidation;

import com.poc.stella_alura.controller.v1.beanvalidation.mapper.EmpresaContractMapper;
import com.poc.stella_alura.controller.v1.beanvalidation.mapper.EmpresaContractRequestMapper;
import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.service.beanvalidation.EmpresaServiceFacade;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Component
@AllArgsConstructor
public class EmpresaContractFacade {

    private final EmpresaServiceFacade empresaServiceFacade;

    public Mono<EmpresaContract> salvarEmpresa(@RequestBody @Valid EmpresaContract empresa) {
        return empresaServiceFacade.salvarEmpresa(
                EmpresaContractRequestMapper.INSTANCE.mapFrom(empresa))
                .map(EmpresaContractMapper.INSTANCE::mapFrom);
    }

}
