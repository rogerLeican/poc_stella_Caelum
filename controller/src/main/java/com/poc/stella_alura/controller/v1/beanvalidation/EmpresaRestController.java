package com.poc.stella_alura.controller.v1.beanvalidation;

import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1")
@AllArgsConstructor
public class EmpresaRestController {

    private final EmpresaContractFacade empresaFacade;

    @PostMapping("/empresa")
    public Mono<EmpresaContract> salvarEmpresa(@RequestBody @Valid EmpresaContract empresa) {
        return empresaFacade.salvarEmpresa(empresa);
    }

}
