package com.poc.stella_alura.controller.v1.beanvalidation.mapper;

import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.service.beanvalidation.model.Empresa;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmpresaContractRequestMapper {

    EmpresaContractRequestMapper INSTANCE = Mappers.getMapper(EmpresaContractRequestMapper.class);

    Empresa mapFrom(EmpresaContract empresaContract);
}
