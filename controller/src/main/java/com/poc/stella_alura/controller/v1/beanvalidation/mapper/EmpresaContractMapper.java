package com.poc.stella_alura.controller.v1.beanvalidation.mapper;

import com.poc.stella_alura.controller.v1.beanvalidation.model.EmpresaContract;
import com.poc.stella_alura.service.beanvalidation.model.Empresa;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmpresaContractMapper {

    EmpresaContractMapper INSTANCE = Mappers.getMapper(EmpresaContractMapper.class);

    EmpresaContract mapFrom(Empresa empresa);
}
