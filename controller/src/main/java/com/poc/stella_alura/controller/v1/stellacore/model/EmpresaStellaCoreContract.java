package com.poc.stella_alura.controller.v1.stellacore.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class EmpresaStellaCoreContract {

    private String razaoSocial;
    private String estado;
    private String ie;
    private String foiValidado;
}
