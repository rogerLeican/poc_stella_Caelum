package com.poc.stella_alura.controller.v1.beanvalidation.model;

import br.com.caelum.stella.bean.validation.IE;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@IE
public class EmpresaContract {

    private String razaoSocial;
    private String estado;
    private String ie;
    private String foiValidado;

}
